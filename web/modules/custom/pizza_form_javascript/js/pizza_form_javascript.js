(function ($, Drupal) {
  Drupal.behaviors.pizzaBehaviour = {
    attach: function (context, settings) {
      var PizzaArray = {};
      var pizza = 0;
      var dist = 0;
      $('#edit-pizza > div > div > select', context).change(function () {
        PizzaArray[$(this).attr('id')] = $(this).children('option:selected').val();
        pizza = 0;
        for (var key in PizzaArray) {
          pizza += PizzaArray[key] * key;
        }
        $("#edit-price").val(Number(Number(pizza) + Number(dist)));
      })
      $('#edit-district > div > input', context).click(function () {
        if ($(this).prop("checked")) {
          dist = settings.pizza.distPrice[Number($(this).attr('value'))]['Price'];
          $("#edit-price").val(Number(Number(pizza) + Number(dist)));
        }
      })

    }
  }
})(jQuery, Drupal);
