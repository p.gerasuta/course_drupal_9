<?php

namespace Drupal\pizza_form_javascript\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PizzaForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pizza_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('pizza_form_javascript.settings');


    $type = $config->get('Type_pizza');
    foreach ($type as $key=>$name){
      if ($name['Available'] === 0)
        unset($type[$key]);
    }

    $form['pizza'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE
    ];

    foreach ($type as $key=>$pizza) {
      $form['pizza'][$key] = [
        '#title' => $pizza['Name'] . '. Цена: ' . $pizza['Price'],
        '#type' => 'select',
        '#options' => range(0, 10),
        '#attributes' => ['id' => $pizza['Price']],
      ];
    }

    $dist = $config->get('District');
    foreach ($dist as $key=>$pizza){
      if ($pizza['Available'] === 0)
        unset($dist[$key]);
      else
        $dist[$key]['Name'] .= '. Цена доставки: ' . $dist[$key]['Price'];
    }

    $form['district'] = [
      '#title' => $this->t('District'),
      '#type' => 'radios',
      '#options' => array_combine(array_keys($dist), array_column($dist, 'Name')),
      '#required' => TRUE
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#required' => TRUE
    ];

    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address to order'),
      '#required' => TRUE
    ];

    $form['price'] = [
      '#type' => 'textfield',
      '#attributes' => array('readonly' => 'readonly'),
    ];

    $form['submit'] = [
      "#type" => "submit",
      '#value' => $this->t('To order'),
    ];

    $form['#attached']['library'][] = 'pizza_form_javascript/pizzaFormJS';
    $form['#attached']['drupalSettings']['pizza']['distPrice'] = $dist;


    return $form;

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $info = $form_state->getUserInput();
    if (empty($form_state->getErrors())) {
      \Drupal::logger('write_log')
        ->notice(serialize($info));
      \Drupal::messenger()->addMessage('Спасибо! Ваша пицца скоро будет доставлена');
    } else {
      \Drupal::logger('write_log')
        ->error($form_state->getErrors());
    }

    $key = 'pizza';
    $to = \Drupal\user\Entity\User::load(1)->getEmail();
    $langcode = "ru";
    $mailManager = \Drupal::service('plugin.manager.mail');
    $params = [];
    $config = \Drupal::config('pizza_form_javascript.settings');
    $type = $config->get('Type_pizza');
    foreach ($type as $key=>$name){
      if ($name['Available'] === 0)
        unset($type[$key]);
    }

    $dist = $config->get('District');
    foreach ($dist as $key=>$name){
      if ($name['Available'] === 0)
        unset($dist[$key]);
    }
    $message = '';
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if (is_array($value)) {
        foreach ($type as $key1=>$name) {
          if ($value[$key1] != 0)
            $message .= $name['Name'] . ' ' . $value[$key1] . ' || ';
        }
      }
      elseif ($key == 'district') {
        $message .= $key . ': ' . $dist[$value]['Name'] . ' || ';
      }
      else {
        $message .= $key . ': ' . $value . ' || ';
      }
    }

    $params['context']['from'] = \Drupal::config('system.site')->get('mail');
    $params['context']['subject'] = 'Pizza order';
    $params['context']['message'] =$message;
    $send = true;
    $result = $mailManager->mail('system', $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      $message = t('There was a problem sending your email notification');
      \Drupal::messenger()->addMessage($message, 'error');
      \Drupal::logger('pizza')->error($message);
    } else {
      $message = t('An email notification has been sent');
      \Drupal::messenger()->addMessage($message, 'status');
      \Drupal::logger('pizza')->notice($message);
      \Drupal::logger('pizza')->notice($params['context']['message']);
    }
  }

}
