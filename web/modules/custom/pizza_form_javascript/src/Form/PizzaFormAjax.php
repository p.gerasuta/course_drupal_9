<?php

namespace Drupal\pizza_form_javascript\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PizzaFormAjax extends FormBase {


  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pizza_form_ajax';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('pizza_form_javascript.settings');
    $type = $config->get('Type_pizza');
    foreach ($type as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($type[$key]);
      }
    }

    $form['pizza'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    foreach ($type as $key => $name) {
      $form['pizza'][$key] = [
        '#title' => $name['Name'] . '. Цена за штуку: ' . $name['Price'],
        '#type' => 'select',
        '#options' => range(0, 10),
        '#ajax' => [
          'callback' => '::myAjaxCallback',
          'disable-refocus' => FALSE,
          'event' => 'change',
          'wrapper' => 'edit-price',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('loading...'),
          ],
        ],
      ];
    }

    $dist = $config->get('District');
    foreach ($dist as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($dist[$key]);
      }
      else {
        $dist[$key]['Name'] .= '. Цена доставки: ' . $dist[$key]['Price'];
      }
    }

    $form['district'] = [
      '#type' => 'radios',
      '#options' => array_combine(array_keys($dist), array_column($dist, 'Name')),
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-price',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('loading...'),
        ],
      ],
      '#required' => TRUE,
      '#title' => $this->t('District'),
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#required' => TRUE,
    ];

    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address to order'),
      '#required' => TRUE,
    ];

    $form['price'] = [
      '#type' => 'textfield',
      '#attributes' => ['readonly' => 'readonly'],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      "#type" => "submit",
      '#value' => $this->t('To order'),
    ];

    return $form;

  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('pizza_form_javascript.settings');
    $type = $config->get('Type_pizza');
    foreach ($type as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($type[$key]);
      }
    }
    $dist = $config->get('District');
    foreach ($dist as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($dist[$key]);
      }
    }
    $typePrice = 0;

    foreach ($type as $key => $pizza) {

      $selected = $form_state->getValue(['pizza', $key]);

      $typePrice += $selected * $pizza['Price'];
    }
    $selected = $dist[$form_state->getValue('district')]['Price'];

    $form['price']['#required'] = TRUE;
    $form['price']['#attributes'] = [
      'id' => 'edit-price',
      'readonly' => 'readonly',
    ];
    $form['price']['#value'] = ($typePrice + $selected);

    return $form['price'];

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $info = $form_state->getUserInput();
    if (empty($form_state->getErrors())) {
      \Drupal::logger('write_log')
        ->notice(serialize($info));
      \Drupal::messenger()
        ->addMessage('Спасибо! Ваша пицца скоро будет доставлена');
    }
    else {
      \Drupal::logger('write_log')
        ->error($form_state->getErrors());
    }

    $key = 'pizzaAJAX';
    $to = \Drupal\user\Entity\User::load(1)->getEmail();
    $langcode = "ru";
    $mailManager = \Drupal::service('plugin.manager.mail');
    $params = [];
    $message = '';
    $config = \Drupal::config('pizza4.settings');
    $type = $config->get('Type_pizza');
    foreach ($type as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($type[$key]);
      }
    }

    $dist = $config->get('District');
    foreach ($dist as $key => $pizza) {
      if ($pizza['Available'] === 0) {
        unset($dist[$key]);
      }
    }

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if (is_array($value)) {
        foreach ($type as $key1 => $pizza) {
          if ($value[$key1] != 0) {
            $message .= $pizza['Name'] . ' ' . $value[$key1] . ' || ';
          }
        }
      }
      elseif ($key == 'district') {
        $message .= $key . ': ' . $dist[$value]['name'] . ' || ';
      }
      else {
        $message .= $key . ': ' . $value . ' || ';
      }
    }
    $params['context']['from'] = \Drupal::config('system.site')->get('mail');
    $params['context']['subject'] = 'Pizza Order';
    $params['context']['message'] = $message;
    $send = TRUE;
    $result = $mailManager->mail('system', $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== TRUE) {
      $message = t('There was a problem sending your email notification');
      \Drupal::messenger()->addMessage($message, 'error');
      \Drupal::logger('pizza')->error($message);
    }
    else {
      $message = t('An email notification has been sent');
      \Drupal::messenger()->addMessage($message, 'status');
      \Drupal::logger('pizza')->notice($message);
      \Drupal::logger('pizza')->notice($params['context']['message']);
    }
  }

}
